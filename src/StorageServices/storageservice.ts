import { Injectable } from '@angular/core';
//import { StorageServiceModule } from 'angular-webstorage-service';

@Injectable()
export class StorageService {
  
  Tokens = [{
    getToken: 'Null'
  }];

  addToken(Tokens: string){
      let token = [];
      token.push(Tokens)
      localStorage.setItem('Tokens', JSON.stringify(token));
  }
  getToken(){
      if (localStorage.getItem('Tokens')!=null){
        this.Tokens = JSON.parse(localStorage.getItem('Tokens'));
        return this.Tokens
      }
  }
  deleteToken(){
    let token = [];
    localStorage.setItem('Tokens', JSON.stringify(token));
  }
}  
