import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { map } from 'rxjs/operators';
import { StorageService } from '../../StorageServices/storageservice'
//import { LoginComponent } from '../components/user/login/login.component';

import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class DataApiService {
  public Token = ''
  constructor(private http: HttpClient, private authService: AuthService, private storage: StorageService) {  }
  Tests: Observable<any>;
  Test: Observable<any>;
  
  public Tokenfull = this.getToken();
  headers: HttpHeaders = new HttpHeaders()

  getAllTest (){
    this.headers = new HttpHeaders ({
      'Content-Type': 'application/json',
      'Authorization': 'Token ' + this.getToken()
    })
    const url_api = 'http://127.0.0.1:8000/api/v1/test1/'
    return this.http.get(url_api, {headers: this.headers})
    .pipe(map(data => data))
  }

  setToken(Token){
    this.storage.addToken(Token);
  }
  getToken(){
    return this.storage.getToken();
  }
  deleteToken(){
    return this.storage.deleteToken();
  }
}
