import { Component, OnInit, ComponentRef } from '@angular/core';
import { DataApiService } from 'src/app/services/data-api.service';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { DashboardInterface } from '../../models/dashboard-interface';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { identifierModuleUrl } from '@angular/compiler';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  public Datos:any = []
  constructor(private dataApiService: DataApiService, private router: Router, private authService: AuthService, private http:HttpClient) { }

  private dash: DashboardInterface = {
    id:'',
    testName:'',
    testPate:'',
    testMate:'',
  }
  ngOnInit() {
    return this.dataApiService.getAllTest()
    .subscribe(data =>{
      this.Datos = data;
    },
    error => {
      console.log(error.error.detail);
    })
  }
//Cierra sesion
  onLogOut(){
    this.dataApiService.deleteToken();
    return this.router.navigate(['/']);
  }

  headers: HttpHeaders = new HttpHeaders()
  
  onEdit(id: HTMLInputElement, testName: HTMLInputElement, testPate: HTMLInputElement, testMate: HTMLInputElement){
    this.headers = new HttpHeaders({
      'Content-type': 'application/json',
      "Authorization": "token " + this.dataApiService.getToken()
    })
    const api_url = 'http://127.0.0.1:8000/api/v1/test1/' + id.value;
    console.log(api_url, testName.value, testPate.value, testMate.value);
    this.http.put(api_url , { 
      testName: testName.value,
      testPate: testPate.value,
      testMate: testMate.value
    }, 
    {
      headers: this.headers})
    .subscribe(
      data =>{
        alert('Guardado correctamente');
        this.ngOnInit();
        //console.log(data);
      },
      error =>{
        alert('Error '+ error.error.detail);
      })
      id.value = '';
      testName.value = '';
      testPate.value = '';
      testPate.value = '';
      id.focus();
  }
  onDelete(id:HTMLInputElement){
    this.headers = new HttpHeaders({
      'Content-type': 'application/json',
      "Authorization": "token " + this.dataApiService.getToken()
    })
    const api_url = 'http://127.0.0.1:8000/api/v1/test1/' + id.value;
    this.http.put(api_url, {
      delete: "true"
    },
    {
      headers: this.headers
    }).subscribe(
      data =>{
        alert('Eliminado satisfactoriamente..');
        this.ngOnInit();
      },
      error =>{
        alert('Error '+ error.error.detail);
      }
    )
    id.value = '';
    id.focus();
    
  }
  //Para este metodo tienen que hacer otra url en django para recuperar lo que han eliminado
  /*onRecover(id:HTMLInputElement){
    this.headers = new HttpHeaders({
      'Content-type': 'application/json',
      "Authorization": "token " + this.dataApiService.getToken()
    })
    const api_url = 'http://127.0.0.1:8000/api/v1/test1/Activate/' + id.value;
    this.http.put(api_url, {
      delete: "false"
    },
    {
      headers: this.headers
    }).subscribe(
      data =>{
        alert('Recuperación satisfactoriamente..');
        this.ngOnInit();
      },
      error =>{
        alert('Error '+ error.error.detail);
      }
    )
    this.ngOnInit();
    id.value = '';
    
    
  }*/
}
