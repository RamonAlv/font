import {MatExpansionModule} from '@angular/material/expansion';

import { NgModule } from '@angular/core';

// @NgModule Es para crear un modulo de angular y hay que importar angular/core
@NgModule({
  imports: [MatExpansionModule],
  exports: [MatExpansionModule],
})
export class MaterialModule { }